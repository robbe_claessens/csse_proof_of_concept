#!/bin/bash

echo "Removing the services...";

kubectl delete deploy keyvault-db
kubectl delete deploy keyvault-server

kubectl delete deploy userservice-db
kubectl delete deploy order-db
kubectl delete deploy userservice-server
kubectl delete deploy userservice-db
kubectl delete deploy order-server


kubectl delete service keyvault-db
kubectl delete service keyvault-server

kubectl delete service userservice-db
kubectl delete service order-db
kubectl delete service userservice-server
kubectl delete service userservice-db
kubectl delete service order-server

if [[ $# -eq 1 ]] && [[ $1 == "all" ]]; then
    echo "Removing the volume...";

    kubectl delete pvc postgres-pvc-order
    kubectl delete pv postgres-pv-keyvault
    kubectl delete pvc postgres-pvc-userservice
fi;

echo "Removing ingress...";

kubectl delete ingress ingress


echo "Please note that it can take a few seconds before all pods were successfully removed..."