#!/usr/bin/env bash



echo "Deploying TalkieWalkie to Kubernetes...";
#a1=""
#a2=""
#options=("y" "n")
#if [[ $# -eq 1 && "${options[@]}" =~ "$1"  ]]; then
#    a1=$1
#    a2=$1
#elif [[ $# -eq 1 && "$1" == "nn" ]]; then
#    a1="n"
#    a2="n"
#elif [[ $# -eq 1 && "$1" == "ny" ]]; then
#    a1="n"
#    a2="y"
#elif [[ $# -eq 1 && "$1" == "yn" ]]; then
#    a1="y"
#    a2="n"
#elif [[ $# -eq 1 && "$1" == "yy" ]]; then
#    a1="y"
#    a2="y"
#elif [[ $# -eq 2 && "${options[@]}" =~ "$1"  && "${options[@]}" =~ "$2"  ]]; then
#    a1=$1
#    a2=$2
#elif [[ $# -ne 0 ]]; then
#    echo "Invalid arguments!"
#    echo "    Usage:  deploy.sh [y|n]"
#    echo "            deploy.sh [y|n][y|n]"
#    echo "            deploy.sh [y|n] [y|n]"
#    exit;
#fi

#while true; do
#    if [[ "$a1" == "" ]]; then
#        read -p "Do you wish to rebuild the frontend? [y/N] " yn
#    else
#        yn=${a1}
#    fi
#    case $yn in
#        [Yy] ) echo "Rebuilding Frontend...";
#            c=$(pwd)
#            cd ../../project/frontendMainAppService/
#            # npm install
#            ./node_modules/@angular/cli/bin/ng build --prod
#            cd ${c};
#            break;;
#        [Nn] ) break;;
#        * )
#            if [[ -n $yn ]]; then echo "Please answer yes or no, not '$yn'"
#            else break
#            fi
#    esac
#done


echo "Changing to minikube docker environment..."

#eval $(minikube docker-env)


echo "Compiling required Dockerfiles..."

#docker build -t messaging-service ../../project/messagingService/
#docker build -t messaging-database ../../project/messagingService/db/
#docker build -t messaging-visualizer ../../project/messagingService/client/
#
#docker build -t profanity-service ../../project/profanityService/
#docker build -t profanity-database ../../project/profanityService/db/
#
#docker build -t posts-service ../../project/postService/
#docker build -t posts-database ../../project/postService/db/
#
#docker build -t comments-service ../../project/commentService/
#docker build -t comments-database ../../project/commentService/db/
#
#docker build -t liking-service ../../project/likingService/
#docker build -t liking-database ../../project/likingService/db/
#
#docker build -t notification-service ../../project/notificationService/
#docker build -t notification-database ../../project/notificationService/db/
#
#docker build -t trail-service ../../project/trailService/
#docker build -t trail-database ../../project/trailService/db/
#
#docker build -t image-service ../../project/imageService/
#docker build -t image-database ../../project/imageService/db/
#
#docker build -t advertisement-service ../../project/advertisementService/
#docker build -t advertisement-database ../../project/advertisementService/db/
#
#docker build -t calendar-service ../../project/calendarService/
#docker build -t calendar-database ../../project/calendarService/db/
#
#docker build -t region-service ../../project/regionService/
#docker build -t region-database ../../project/regionService/db/
#
#docker build -t auth-service ../../project/authService/
#
#docker build -t frontend-visualizer ../../project/frontendMainAppService/

#while true; do
#    if [[ "$a2" == "" ]]; then
#        read -p "Do you wish to enable minikube? [y/N] " yn
#    else
#        yn=${a2}
#    fi
#    case $yn in
#        [Yy] )  minikube start;
#            break;;
#        [Nn] ) break;;
#        * )
#            if [[ -n $yn ]]; then echo "Please answer yes or no, not '$yn'"
#            else break
#            fi
#    esac
#done

echo "Applying the volumes..."

kubectl apply -f ./setup/pv/pv-mysql-order.yml
kubectl apply -f ./setup/pv/pv-mysql-userservice.yml


echo "Creating the database credentials..."

# kubectl apply -f ./setup/secret.yml


echo "Creating the services and deployments..."

# Do the authService first, seeing as that takes the longest
kubectl create -f ./setup/orderServer.yml
kubectl create -f ./setup/keyvaultServer.yml
kubectl create -f ./setup/userService.yml
# kubectl expose pod auth --type=NodePort --port=80

echo "Setting the ingress..."

minikube addons enable ingress
kubectl apply -f ./setup/ingress.yml


#while true; do
#    if [[ "$a2" == "" ]]; then
#        read -p "Do you wish to set the valid hosts in /etc/hosts? [y/N] " yn
#    else
#        yn=${a2}
#    fi
#    case $yn in
#        [Yy] )
#            echo "$(minikube ip) talkiewalkie.io" | sudo tee -a /etc/hosts
#            echo "$(minikube ip) messenger.talkiewalkie.io" | sudo tee -a /etc/hosts
#            break;;
#        [Nn] ) break;;
#        * )
#            if [[ -n $yn ]]; then echo "Please answer yes or no, not '$yn'"
#            else break
#            fi
#    esac
#done
