<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getUser($token)
    {
        $user = User::where('token', $token)->first();
        return response()->json([
            'user' => $user ? $user : null,
        ]);
    }
}
