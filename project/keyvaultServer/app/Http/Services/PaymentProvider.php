<?php

namespace App\Http\Services;

// Payment provider service to mock out a real payment provider
class PaymentProvider {

    public function pay($amount) {
        // Mock out the payment processing with sleep
        sleep(1.5);
        return true;
    }


}
