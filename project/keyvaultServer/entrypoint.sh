#!/usr/bin/env bash

# Must be done every time because we don't know the valid keys otherwise
#echo "" >> .env
#php artisan passport:install | awk -v OFS="=" '/P(ersonal|assword)/ { K = toupper($1); next } /Client/ { if($3 ~/^[0-9]+$/) { key=K"_CLIENT_ID" } else { key=K"_CLIENT_SECRET" } print key,$3 }' >> .env

docker-php-entrypoint php-fpm
