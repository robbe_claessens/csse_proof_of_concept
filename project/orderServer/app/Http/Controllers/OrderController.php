<?php

namespace App\Http\Controllers;

use App\Http\Services\PaymentProvider;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    private $paymentProvider;

    public function __construct()
    {
        $this->paymentProvider = new PaymentProvider();
    }

    private function ticketsAvailable($ticketsNeeded) {
        return (80 - (Order::sum('number_of_tickets') + $ticketsNeeded)) >= 0;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'tickets_sold' => Order::sum('number_of_tickets')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $starttime = microtime(true);

        // Make request to auth service to verify user
        $client = new Client(); //GuzzleHttp\Client
        $result = $client->get('http://userservice-server:80/api/getUser/token');
        usleep(30);


        $number_of_tickets = $request->number_of_tickets;
        if ($number_of_tickets <= 0 || $number_of_tickets > 4) {
            return response()->json([
                'message' => 'You can order max 4 tickets.'
            ]);
        }

        // Check balance, if ok then charge
        DB::beginTransaction();

        // Create the order
        $order = Order::make([
            'number_of_tickets' => $number_of_tickets
        ]);

        // Check if tickets available
        if ($this->ticketsAvailable($order->number_of_tickets) === false) {
            return response()->json([
                'message' => 'No tickets avialable.'
            ]);
        }

        // Access keyvault for payment information
        $client = new Client(); //GuzzleHttp\Client
        $result = $client->get('http://keyvault-server:80/api/key/lala');
//        usleep(30);

        // Make payment
        if (!$this->paymentProvider->pay(500 * $order->number_of_tickets)) {
            DB::rollBack();
            return response()->json([
                'message' => 'Not enough money on your card.'
            ]);
        }

        $order->save();

        DB::commit();

        $endtime = microtime(true);
        $timediff = $endtime - $starttime;

        // All went succesfuly
        return response()->json([
            'message' => 'Succesfuly Completed!',
            'time-elapsed' => $timediff
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
