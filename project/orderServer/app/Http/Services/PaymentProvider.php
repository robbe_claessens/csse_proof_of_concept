<?php

namespace App\Http\Services;

// Payment provider service to mock out a real payment provider
class PaymentProvider {

    private function float_rand($min, $max, $round=0){
        //validate input
        if ($min>$max) { $min=$max; $max=$min; }
            else { $min=$min; $max=$max; }
        $randomfloat = $min + mt_rand() / mt_getrandmax() * ($max - $min);
        if($round>0)
            $randomfloat = round($randomfloat,$round);

        return $randomfloat;
    }


    public function pay($amount) {
        // Mock out the payment processing with sleep
        $sleep = intval($this->float_rand(2, 5));
        sleep($sleep);
        return true;
    }


}
